<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Brand\Brand;

class Car implements Product
{
    private string $name;
    private Brand $brand;
    private int $productNumber;
    private int $price;
    private string $color;

    /**
     * @param string $name
     * @param Brand $brand
     * @param int $productNumber
     * @param int $price
     * @param string $color
     */
    public function __construct(string $name, Brand $brand, int $productNumber, int $price, string $color)
    {
        $this->name = $name;
        $this->brand = $brand;
        $this->productNumber = $productNumber;
        $this->price = $price;
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->getBrand()->getName() . " " . $this->getName() . ", " . $this->getPrice();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return int
     */
    public function getProductNumber(): int
    {
        return $this->productNumber;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        $classname = explode('\\', self::class);
        return $classname[sizeof($classname) - 1];
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }
}
