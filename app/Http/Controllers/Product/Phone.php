<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Brand\Brand;

class Phone implements Product
{
    private string $name;
    private Brand $brand;
    private int $productNumber;
    private int $price;

    /**
     * @param string $name
     * @param Brand $brand
     * @param int $productNumber
     * @param int $price
     */
    public function __construct(string $name, Brand $brand, int $productNumber, int $price)
    {
        $this->name = $name;
        $this->brand = $brand;
        $this->productNumber = $productNumber;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getInfo(): string
    {
        return $this->getBrand()->getName() . ' ' . $this->getName() . " " . $this->getPrice();
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Brand
     */
    public function getBrand(): Brand
    {
        return $this->brand;
    }

    /**
     * @return int
     */
    public function getProductNumber(): int
    {
        return $this->productNumber;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        $classname = explode('\\', self::class);
        return $classname[sizeof($classname) - 1];
    }
}
