<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Brand\Brand;

interface Product
{

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return Brand
     */
    public function getBrand(): Brand;

    /**
     * @return int
     */
    public function getProductNumber(): int;

    /**
     * @return int
     */
    public function getPrice(): int;

    /**
     * @return string
     */
    public function getInfo(): string;

    /**
     * @return string
     */
    public function getType(): string;
}
