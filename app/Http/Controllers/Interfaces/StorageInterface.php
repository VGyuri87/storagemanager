<?php

namespace App\Http\Controllers\Interfaces;

use App\Http\Controllers\Product\Car;
use App\Http\Controllers\Product\Phone;
use App\Http\Controllers\Product\Product;

interface StorageInterface
{
    /**
     * @param Car $car
     * @return void
     */
    public function storeCar(Car $car): void;

    /**
     * @param Phone $phone
     * @return void
     */
    public function storePhone(Phone $phone): void;

    /**
     * @return array
     */
    public function getProducts(): array;

    /**
     * @param Product $product
     * @return void
     */
    public function removeProduct(Product $productNumber): void;
}
