<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Brand\Brand;
use App\Http\Controllers\Product\Car;
use App\Http\Controllers\Product\Phone;
use App\Http\Controllers\Storage\Storage1;

class StorageController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * @throws \Exception
     */
    public function index()
    {
        $storage = array();
        $newCars = array();
        $newPhones = array();

        $brand1 = new Brand('Opel', 4);
        $newCars[] = new Car('Omega', $brand1, 5635766387, 2000000, 'red');
        $newCars[] = new Car('Kadett', $brand1, 5635763621, 1000000, 'green');
        $newCars[] = new Car('Corsa', $brand1, 5635700876, 900000, 'yellow');
        $brand2 = new Brand('Iphone', 5);
        $newPhones[] = new Phone('Se', $brand2, 5136874931, 160000);
        $newPhones[] = new Phone('5s', $brand2, 5136874945, 300000);

        $storage[] = new Storage1('Raktar1', 'Budapest', 3);
        $storage[] = new Storage1('Raktar2', 'Pécs', 23);

        $foundStorage = false;
        foreach ($storage as $stor) {
            if($stor->getCapacity()-$stor->getStock() > sizeof($newCars) && !$foundStorage){
                for ($i =0;$i < sizeof($newCars);$i++){
                    $stor->storeCar($newCars[$i]);
                }
                $foundStorage = true;
            }
        }

        $foundStorage = false;
        foreach ($storage as $stor) {
            if($stor->getCapacity()-$stor->getStock() > sizeof($newPhones) && ! $foundStorage){
                for ($i =0;$i < sizeof($newPhones);$i++){
                    $stor->storePhone($newPhones[$i]);
                }
                $foundStorage = true;
            }
        }

        //$storage->removeProduct(new Car('Corsa', $brand1, 5635700876, 900000, 'yellow'));
        $stores = array();
        foreach ($storage as $stor) {
            $stores[] = $stor->getProducts();
        }

        return view('index', ['stores' => $stores]);
    }
}
