<?php

namespace App\Http\Controllers\Brand;

class Brand
{
    private string $name;
    private int $qualityCategory;

    /**
     * @param string $name
     * @param int $qualityCategory
     */
    public function __construct(string $name, int $qualityCategory)
    {
        $this->name = $name;
        $this->qualityCategory = $qualityCategory;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getQualityCategory(): int
    {
        return $this->qualityCategory;
    }
}
