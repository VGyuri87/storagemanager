<?php

namespace App\Http\Controllers\Storage;

class BaseStorage
{
    protected string $name;
    protected string $address;
    protected int $capacity;
    protected int $stock;


    /**
     * @param string $name
     * @param string $address
     * @param int $capacity
     */
    public function __construct(string $name, string $address, int $capacity, int $stock = 0)
    {
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return $this->capacity;
    }

    /**
     * @return int
     */
    public function getStock(): int
    {
        return $this->stock;
    }

}
