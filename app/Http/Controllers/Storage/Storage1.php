<?php

namespace App\Http\Controllers\Storage;

use App\Http\Controllers\Interfaces\StorageInterface;
use App\Http\Controllers\Product\Car;
use App\Http\Controllers\Product\Phone;
use App\Http\Controllers\Product\Product;

class Storage1 extends BaseStorage implements StorageInterface
{

    private array $cars = array();
    private array $phones = array();

    /**
     * @param Car $car
     * @return void
     * @throws \Exception
     */
    public function storeCar(Car $car): void
    {
        if ($this->capacity > ($this->stock + 1)) {
            $this->stock++;
            $this->cars[] = $car;
        } else {
            throw new \Exception('Ez a termék már nem fér bele a raktárkészletbe: ' . $car->getInfo());
        }
    }

    /**
     * @param Phone $phone
     * @return void
     * @throws \Exception
     */
    public function storePhone(Phone $phone): void
    {
        if ($this->capacity > ($this->stock + 1)) {
            $this->stock++;
            $this->phones[] = $phone;
        } else {
            throw new \Exception('Ez a termék már nem fér bele a raktárkészletbe: ' . $phone->getInfo());
        }
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return array_merge($this->cars, $this->phones);
    }

    /**
     * @param Product $productNumber
     * @return void
     */
    public function removeProduct(Product $product): void
    {
        switch ($product->getType()) {
            case 'Car':
                $this->removeCarByProductNumber($product->getProductNumber());
                break;
            case 'Phone':
                $this->removePhoneByProductNumber($product->getProductNumber());
                break;
        }

    }

    /**
     * @param int $productNumber
     * @return void
     */
    private function removeCarByProductNumber(int $productNumber): void
    {
        foreach ($this->cars as $key => $car) {
            if ($productNumber == $car->getProductNumber()) {
                unset($this->cars[$key]);
            }
        }
    }

    /**
     * @param int $productNumber
     * @return void
     */
    private function removePhoneByProductNumber(int $productNumber): void
    {
        foreach ($this->phones as $key => $phone) {
            if ($productNumber == $phone->getProductNumber()) {
                unset($this->$phone[$key]);
            }
        }
    }
}
