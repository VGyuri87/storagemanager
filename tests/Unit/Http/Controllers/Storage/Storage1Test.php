<?php

namespace Http\Controllers\Storage;

use App\Http\Controllers\Brand\Brand;
use App\Http\Controllers\Interfaces\StorageInterface;
use App\Http\Controllers\Product\Car;
use App\Http\Controllers\Product\Phone;
use App\Http\Controllers\Storage\Storage1;
use PHPUnit\Framework\TestCase;

class Storage1Test extends TestCase
{
    public StorageInterface $storte;

    protected function setUp(): void
    {
        parent::setUp();
        $this->storte = new Storage1('Store1', 'Budapest', 7);
    }


    /**
     * @depends testStoreCar
     * @return void
     */
    public function testGetProducts()
    {
        $brand1 = new Brand('Opel', 4);
        $newCars1 = new Car('Omega', $brand1, 5635766387, 2000000, 'red');
        $newCars2 = new Car('Kadett', $brand1, 5635763621, 1000000, 'green');
        $newCars3 = new Car('Corsa', $brand1, 5635700876, 900000, 'yellow');
        $this->storte->storeCar($newCars1);
        $this->storte->storeCar($newCars2);
        $this->storte->storeCar($newCars3);

        $brand2 = new Brand('Iphone', 4);
        $newPhone1 = new Phone('Se', $brand2, 56357668737, 2000000);
        $newPhone2 = new Phone('5s', $brand2, 56357637473, 1000000);
        $newPhone3 = new Phone('12', $brand2, 5633460876, 900000);
        $this->storte->storePhone($newPhone1);
        $this->storte->storePhone($newPhone2);
        $this->storte->storePhone($newPhone3);

        $this->assertEquals(
            [
                new Car('Omega', $brand1, 5635766387, 2000000, 'red'),
                new Car('Kadett', $brand1, 5635763621, 1000000, 'green'),
                new Car('Corsa', $brand1, 5635700876, 900000, 'yellow'),
                new Phone('Se', $brand2, 56357668737, 2000000),
                new Phone('5s', $brand2, 56357637473, 1000000),
                new Phone('12', $brand2, 5633460876, 900000)
            ], $this->storte->getProducts());
    }

    /**
     * @depends testStoreCar
     * @return void
     */
    public function testRemoveProduct()
    {
        $brand1 = new Brand('Opel', 4);
        $newCars1 = new Car('Omega', $brand1, 5635766387, 2000000, 'red');
        $newCars2 = new Car('Kadett', $brand1, 5635763621, 1000000, 'green');
        $newCars3 = new Car('Corsa', $brand1, 5635700876, 900000, 'yellow');
        $this->storte->storeCar($newCars1);
        $this->storte->storeCar($newCars2);
        $this->storte->storeCar($newCars3);
        $this->storte->removeProduct($newCars2);


        $this->assertEquals([
            new Car('Omega', $brand1, 5635766387, 2000000, 'red'),
            new Car('Corsa', $brand1, 5635700876, 900000, 'yellow')], $this->storte->getProducts());
    }

    /**
     * @return void
     */
    public function testStoreCar()
    {
        $brand1 = new Brand('Opel', 4);
        $newCars1 = new Car('Omega', $brand1, 5635766387, 2000000, 'red');
        $newCars2 = new Car('Kadett', $brand1, 5635763621, 1000000, 'green');
        $newCars3 = new Car('Corsa', $brand1, 5635700876, 900000, 'yellow');
        $this->storte->storeCar($newCars1);
        $this->storte->storeCar($newCars2);
        $this->storte->storeCar($newCars3);

        $this->assertEquals([
            new Car('Omega', $brand1, 5635766387, 2000000, 'red'),
            new Car('Kadett', $brand1, 5635763621, 1000000, 'green'),
            new Car('Corsa', $brand1, 5635700876, 900000, 'yellow')], $this->storte->getProducts());
    }

    /**
     *
     * @return void
     */
    public function testStorePhone()
    {
        $brand1 = new Brand('Iphone', 4);
        $newPhone1 = new Phone('Se', $brand1, 56357668737, 2000000);
        $newPhone2 = new Phone('5s', $brand1, 56357637473, 1000000);
        $newPhone3 = new Phone('12', $brand1, 5633460876, 900000);
        $this->storte->storePhone($newPhone1);
        $this->storte->storePhone($newPhone2);
        $this->storte->storePhone($newPhone3);

        $this->assertEquals([
            new Phone('Se', $brand1, 56357668737, 2000000),
            new Phone('5s', $brand1, 56357637473, 1000000),
            new Phone('12', $brand1, 5633460876, 900000)], $this->storte->getProducts());
    }

    public function testStoreWithLittleSpace()
    {
        $this->expectException(\Exception::class);

        $store = new Storage1('Store1', 'Budapest', 2);
        $brand1 = new Brand('Iphone', 4);
        $newPhone1 = new Phone('Se', $brand1, 56357668737, 2000000);
        $newPhone2 = new Phone('5s', $brand1, 56357637473, 1000000);
        $newPhone3 = new Phone('12', $brand1, 5633460876, 900000);
        $store->storePhone($newPhone1);
        $store->storePhone($newPhone2);
        $store->storePhone($newPhone3);
    }
}
